﻿using System;
using GAF;
using GAF.Operators;
using Math = System.Math;

namespace GeneticAlgorithm
{
	/// <summary>
	///  Genetic Algorithm from https://gaframework.org/wiki/index.php/Solving_the_Binary_F6_Function
	/// </summary>
	public class BinaryF6FunctionExample
	{
		private readonly double crossoverProbability = 0.85;
		private readonly double mutationProbability = 0.08;
		private readonly int elitismPercentage = 5;

		public void Execute()
		{
			//create a Population of 100 random chromosomes of length 44
			var population = new Population(100, 44, false, false);

			//create the genetic operators
			var elite = new Elite(elitismPercentage);
			var crossover = new Crossover(crossoverProbability, true) { CrossoverType = CrossoverType.SinglePoint };
			var mutation = new BinaryMutate(mutationProbability, true);

			//create the GA itself
			var algorithm = new GAF.GeneticAlgorithm(population, EvaluateFitness);

			//subscribe to the GAs Generation Complete event
			algorithm.OnGenerationComplete += OnGenerationComplete;
			algorithm.OnRunComplete += (sender, args) => Console.WriteLine("Zakończono generowanie");

			//add the operators to the ga process pipeline
			algorithm.Operators.Add(elite);
			algorithm.Operators.Add(crossover);
			algorithm.Operators.Add(mutation);

			//run the GA
			algorithm.Run(TerminateAlgorithm);
		}

		private double EvaluateFitness(Chromosome chromosome)
		{
			if (chromosome == null)
				throw new ArgumentNullException("chromosome", "The specified Chromosome is null.");

			//this is a range constant that is used to keep the x/y range between -100 and +100
			double rangeConst = 200 / (Math.Pow(2, chromosome.Count / 2) - 1);

			//get x and y from the solution
			var x1 = Convert.ToInt32(chromosome.ToBinaryString(0, chromosome.Count / 2), 2);
			var y1 = Convert.ToInt32(chromosome.ToBinaryString(chromosome.Count / 2, chromosome.Count / 2), 2);

			//Adjust range to -100 to +100
			var x = (x1 * rangeConst) - 100;
			var y = (y1 * rangeConst) - 100;

			//using binary F6 for fitness.
			var temp1 = Math.Sin(Math.Sqrt(x * x + y * y));
			var temp2 = 1 + 0.001 * (x * x + y * y);
			var result = 0.5 + (temp1 * temp1 - 0.5) / (temp2 * temp2);

			return 1 - result;
		}

		private bool TerminateAlgorithm(Population population, int currentGeneration, long currentEvaluation)
		{
			return currentGeneration > 1000;
		}

		private void OnGenerationComplete(object sender, GaEventArgs e)
		{
			//get the best solution
			var chromosome = e.Population.GetTop(1)[0];

			//decode chromosome

			//get x and y from the solution
			int x1 = Convert.ToInt32(chromosome.ToBinaryString(0, chromosome.Count / 2), 2);
			int y1 = Convert.ToInt32(chromosome.ToBinaryString(chromosome.Count / 2, chromosome.Count / 2), 2);

			//Adjust range to -100 to +100
			double rangeConst = 200 / (Math.Pow(2, chromosome.Count / 2) - 1);
			double x = (x1 * rangeConst) - 100;
			double y = (y1 * rangeConst) - 100;

			//display the X, Y and fitness of the best chromosome in this generation
			Console.WriteLine("x:{0:0.000} y:{1:0.000} Fitness{2:0.000}", x, y, e.Population.MaximumFitness);
		}
	}
}