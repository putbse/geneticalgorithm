﻿using System;

namespace GeneticAlgorithm
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			var example = new BinaryF6FunctionExample();
			example.Execute();

			Console.ReadKey();
		}
	}
}